# ⚠️ MIGRATION NOTICE ⚠️

This repository is DEPRECATED, it has been moved over to GitHub. This should resolve the HTTP 429 errors when downloading/updating, and provides much faster speeds in general.

The new repository is hosted at https://twelho.github.io/freecad-realthunder/.

You **will need to reinstall** the `.flatpakrepo` to have it point to the correct URL.
